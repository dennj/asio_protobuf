#include "pch.h"
#include "student.pb.h"

void Printstudent(const student::Student& student_message) {
  std::cout << "N of courses: " << student_message.courses_size() << "\n"
            << "\nStudent id: " << student_message.id() << "\nName: " << student_message.name()
            << "\nGeneration time: " << google::protobuf::util::TimeUtil::ToString(student_message.last_updated())
            << std::endl;

  for (auto& course : student_message.courses()) {
    std::cout << "Course ID: " << course.id() << "\n Exam grade: " << course.exam_grade() << std::endl;

    for (auto& homehork : course.homework()) {
      std::cout << "\tHomework ID: " << homehork.id() << "\n\t\tGrade: " << homehork.grade() << std::endl;
    }
  }
}

int main(int argc, char* argv[]) {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  student::Student student_message;

  try {
    if (argc != 2) {
      std::cerr << "Please write the server IP addres as an argument: client <server IP address>"
                   "\n e.g. client localhost\n";
      return 1;
    }

    boost::asio::io_context io_context;  // Basic I/O services.

    // Create a resolver object to turn the server name address into a TCP endpoint.
    boost::asio::ip::tcp::resolver resolver(io_context);
    boost::asio::ip::tcp::resolver::results_type endpoints = resolver.resolve(argv[1], "daytime");

    boost::asio::ip::tcp::socket socket(io_context);  // Create a socket.
    boost::asio::connect(socket, endpoints);  // Use the socket to reaches out to the server to form a connection.

    boost::system::error_code error;
    while (error != boost::asio::error::eof)  // Run untill the connection is closed cleanly by peer.
    {
      boost::array<char, 128> buf{};  // Buffer to store the message received
      size_t len = socket.read_some(boost::asio::buffer(buf), error);

      if (error && error != boost::asio::error::eof) {
        throw boost::system::system_error(error);
      }

      if (error != boost::asio::error::eof) {
        std::cout << "Message length: " << len << "bytes" << std::endl;

        student_message.ParseFromArray(buf.data(), static_cast<int32_t>(len));

        Printstudent(student_message);
      }
    }
  } catch (std::exception& e) {
    std::cerr << "ERROR An execption has been thrown: " << e.what() << std::endl;
  }

  // Optional:  Delete all global objects allocated by libprotobuf.
  google::protobuf::ShutdownProtobufLibrary();
  return 0;
}
