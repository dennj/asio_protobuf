#include "pch.h"
#include "student.pb.h"

void set_student(student::Student* student_message) {
  student_message->set_id(100117);
  student_message->set_name("Dennj Osele");
  student_message->set_year(3);

  *student_message->mutable_last_updated() = google::protobuf::util::TimeUtil::SecondsToTimestamp(time(nullptr));

  for (int i = 0; i < 3; ++i) {
    student::Course course;
    course.set_id(i);
    course.set_exam_grade(82);

    // Set 3 homeworks
    for (int j = 0; j < 3; ++j) {
      student::Course_Homework homework;
      homework.set_id(j);
      homework.set_grade(75);

      *course.add_homework() = std::move(homework);
    }
    *student_message->add_courses() = course;
  }
}

int main() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  student::Student student_message;

  set_student(&student_message);

  try {
    boost::asio::io_context io_context;  // Basic I/O services.

    // TCP acceptor object needs to be created to listen for new connection on TCP port 13, for IPv4.
    boost::asio::ip::tcp::acceptor acceptor(io_context, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), 13));

    std::cout << "Listening for connections" << std::endl;

    while (true) {
      boost::asio::ip::tcp::socket socket(io_context);
      acceptor.accept(socket);  // Wait for a connection

      // A client connected to the server.
      std::cout << "Client sent a request from the address: "
                << boost::lexical_cast<std::string>(socket.remote_endpoint()) << "\n";

      // Serialize struct into a string
      std::ostringstream stream;
      student_message.SerializeToOstream(&stream);
      std::string text = stream.str();

      // Send string to client
      boost::system::error_code ignored_error;
      boost::asio::write(socket, boost::asio::buffer(text), ignored_error);

      if (ignored_error && ignored_error != boost::asio::error::message_size) {
        throw boost::system::system_error(ignored_error);
      }
    }
  } catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
  }

  // Optional:  Delete all global objects allocated by libprotobuf.
  google::protobuf::ShutdownProtobufLibrary();

  return 0;
}
