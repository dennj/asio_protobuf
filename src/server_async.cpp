#include "pch.h"
#include "student.pb.h"

void set_student(student::Student* student_message) {
  student_message->set_id(100117);
  student_message->set_name("Dennj Osele");
  student_message->set_year(3);

  *student_message->mutable_last_updated() = google::protobuf::util::TimeUtil::SecondsToTimestamp(time(nullptr));

  for (int i = 0; i < 3; ++i) {
    student::Course course;
    course.set_id(i);
    course.set_exam_grade(82);

    // Set 3 homeworks
    for (int j = 0; j < 3; ++j) {
      student::Course_Homework homework;
      homework.set_id(j);
      homework.set_grade(75);

      *course.add_homework() = std::move(homework);
    }
    *student_message->add_courses() = course;
  }
}

class tcp_connection : public boost::enable_shared_from_this<tcp_connection> {
 public:
  static boost::shared_ptr<tcp_connection> create(boost::asio::io_context& io_context) {
    return boost::shared_ptr<tcp_connection>(new tcp_connection(io_context));
  }

  boost::asio::ip::tcp::socket& socket() { return socket_; }

  void start() {
    student::Student student_message;

    set_student(&student_message);
    // Serialize struct into a string
    std::ostringstream stream;
    student_message.SerializeToOstream(&stream);
    std::string text = stream.str();

    // Send string to client
    boost::asio::async_write(
        socket_, boost::asio::buffer(text),
        boost::bind(&tcp_connection::handle_write, shared_from_this(), boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred));
  }

 private:
  tcp_connection(boost::asio::io_context& io_context) : socket_(io_context) {}

  void handle_write(const boost::system::error_code& /*error*/, size_t /*bytes_transferred*/) {}

  boost::asio::ip::tcp::socket socket_;
  std::string message_;
};

class tcp_server {
 public:
  tcp_server(boost::asio::io_context& io_context)
      : acceptor_(io_context, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), 13)) {
    start_accept();
  }

 private:
  void start_accept() {
    boost::shared_ptr<tcp_connection> new_connection = tcp_connection::create(acceptor_.get_executor().context());

    acceptor_.async_accept(new_connection->socket(), boost::bind(&tcp_server::handle_accept, this, new_connection,
                                                                 boost::asio::placeholders::error));
  }

  void handle_accept(const boost::shared_ptr<tcp_connection>& new_connection, const boost::system::error_code& error) {
    if (!error) {
      // A client connected to the server.
      std::cout << "Client sent a request from the address: "
                << boost::lexical_cast<std::string>(new_connection->socket().remote_endpoint()) << "\n";
      new_connection->start();
    }

    start_accept();
  }

  boost::asio::ip::tcp::acceptor acceptor_;
};

int main() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;
  try {
    boost::asio::io_context io_context;  // Basic I/O services.
    tcp_server server(io_context);
    std::cout << "Listening for connections" << std::endl;
    io_context.run();
  } catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
  }

  return 0;
}
